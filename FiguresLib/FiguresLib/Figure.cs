﻿namespace FiguresLib
{
    public static class Figure
    {
        public static double Area(double[] a)
        {
            if (a.Length == 1)
            {
                return Circle.Area(a[0]);
            }
            if (a.Length == 3)
            {
                return Triangle.Area(a[0], a[1], a[2]);
            }
            return 0;
        }            
    }
}

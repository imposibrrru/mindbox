﻿using System;

namespace FiguresLib
{
    public static class Triangle
    {
        public static double Area(double a, double b, double c)
        {
            var hp = (a + b + c) / 2;
            return Math.Sqrt(hp * (hp - a) * (hp - b) * (hp - c));
        }

        public static bool IsRectangle(double a, double b, double c)
        {
            double[] sides = new double[] { a, b, c };
            for (int i = 1; i < sides.Length; i++)
            {
                if (sides[i] > sides[0])
                {
                    var temp = sides[0];
                    sides[0] = sides[i];
                    sides[i] = temp;
                }
            }

            if (Math.Pow(sides[0],2) == Math.Pow(sides[1], 2) + Math.Pow(sides[2], 2))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}

﻿using System;

namespace FiguresLib
{
    public static class Circle
    {
        public static double Area(double r)
        {
            return Math.PI * (r * r);
        }
    }
}
